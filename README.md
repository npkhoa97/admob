# Hướng Dẫn Admob

### Bước 1: cài Admob


    yarn add react-native-admob@next
    
    
### Bước 2: Chạy `react-native-link` để Android tự động link thư viện
### Bước 3: Config Admob cho IOS
- Thêm Mobile Ads SDK bằng Cocoapods
  - Cài Cocoapods:
    - chạy `cd ~ && sudo gem install cocoapods`
    - chạy `pod setup`
  - Setup Mobile Ads
    - vào thư mục ios của project và chạy `pod init`
    - sửa `Podfile` và ios/<tên_project>/AppDelegate.m:
    
    
    
```
// Podfile
platform :ios, '9.0'
use_frameworks!
target 'admob' do
  pod 'Google-Mobile-Ads-SDK'
end
```

```
// AppDelegate.m
// ...
@import GoogleMobileAds;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  // ...
  [GADMobileAds configureWithApplicationID:@"<ADMOB_ID_CỦA_MÌNH>"];
  // ví dụ: [GADMobileAds configureWithApplicationID:@"ca-app-pub-3940256099942544~1458002511"];
  return YES;
}

@end
```
    
### Bước 4: Chèn ads
```
import React, {Component} from 'react';
import {View} from 'react-native';
import {
  AdMobBanner,
} from 'react-native-admob'

export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <AdMobBanner
          adSize="fullBanner"
          adUnitID="ca-app-pub-3940256099942544/2934735716"
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error)}
        />
      </View>
    );
  }
}
```
 